<a name="1.0"></a>
# [1.0.0](https://github.com/mindslab-ai/brain-stt-train/compare/760dde5...v1.0.0) (2017-12-08)
<!---
 MAJOR.MINOR 패치는 # 즉, H1으로 표시
 MAJOR>MINOR.PATCH는 ## 즉, H2로 표시한다.
--->

### Bug Fixes

None

### Features

* **proto:** maum.brain.stt.train 패키지로 재구성
* **config:** brain-stt-train 패키지로 재구성
* **s3evaluation:** stt 평가 서버
* **stt-trainer:** stt 학습 서버

### Enhancements

None

### Breaking Changes

* 네임스페이스 변경, 실행 서버와 학습 서버의 분리 정책에 따라서 재구성
