# maum.ai BRAIN STT TRAIN

## main.ai brain stt train servers on grpc

## dependency 
- protobuf
- grpc
- spdlog
- libmaum-json
- libmaum-common


## install cuda
### get cuda
- https://developer.nvidia.com/cuda-downloads
- Select valid version for your OS

```bash
sudo dpkg -i cuda-repo-ubuntu1604_8.0.44-1_amd64.deb
sudo apt-get update
sudo apt-get install cuda
sudo apt-get install libarchive libarchive-dev
```

### atlas
```bash
sudo apt install libatlas-base-dev libatlas-dev
```

## install python boto3
### pip
```bash
sudo apt-get install python-pip python-dev build-essential 
sudo pip install --upgrade pip 
sudo pip install --upgrade virtualenv 
```

### boto3
```bash
sudo pip install boto3
```


## How to build

### Pre-requisite
- libmaum should be built first.

```bash
mkdir ~/git
cd ~/git
git clone git@github.com:mindslab-ai/brain-stt

cd brian-stt
mkdir build-debug-4.8
cd build-debug-4.8
cmake \
  -DCMAKE_PREFIX_PATH=~/maum \
  -DCMAKE_INSTALL_PREFIX=~/maum \
  -DCMAKE_BUILD_TYPE=Debug \
  ..
make
make install
```

### Supported compiler
- gcc 5
- gcc 4.8 or later

## TODO
- installing openmpi related binaries
