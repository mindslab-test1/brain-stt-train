# -*- coding: utf-8 -*-

import sys

reload(sys)

from common.config import Config
from maum.brain.stt.train import eval_pb2
from maum.brain.stt.train import eval_pb2_grpc
from stt_evaluation import SttEvaluation

from concurrent import futures
import time

import grpc

_ONE_DAY_IN_SECONDS = 60 * 60 * 24


def serve():
    conf = Config()
    conf.init('brain-stt-train.conf')
    print 'eval server init'
    data = [
        ('grpc.max_connection_idle_ms', int(conf.get('brain-stt.evald.front.port'))),
        ('grpc.max_connection_age_ms', int(conf.get('brain-stt.evald.front.port')))
    ]
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10), options=data)
    eval_pb2_grpc.add_SttEvalModelResolverServicer_to_server(SttEvaluation(), server)
    server.add_insecure_port('[::]:' + conf.get('brain-stt.evald.front.port'))
    server.start()
    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)
