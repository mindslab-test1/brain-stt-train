# -*- coding: utf-8 -*-
import sys, os

reload(sys)

import string
from common.config import Config


class WordToSyllable:
    def __init__(self):
        self.conf = Config()
        self.conf.init('brain-stt-train.conf')
        self.RESOURCE_PATH = self.conf.get('brain-stt.eval.resource.evaluation')
        self.prj_name = 'tempdist'

    def make_syl_a(self, word):
        if word == 'sil':
            syl_a = [word]
            return syl_a

        word_len = len(word)
        cc = 0
        syl_a = []
        flag = 0
        while cc < word_len:
            ch = word[cc]
            if ch.isdigit() == 1 or ch.isalpha() == 1:
                # print 'add ch : %c (%s)' % (ch, word)
                if word not in self.sym_a:
                    self.sym_a[word] = 1
            if ch.isdigit() == 1:
                ch = 'dd_%s' % ch
                syl_a.append(ch)
                cc += 1
                flag = 1
            else:
                syl = word[cc:cc + 2]
                syl_a.append(syl)
                cc += 2
        return syl_a

    def word_check(self, syll_out, word_out2, sym_out3):

        for line in file(self.in_fn2):
            line = string.strip(line)

            if '=' not in line:
                continue
            item_a = line.split('=')
            word_org = item_a[0]
            word_tar = item_a[1]
            word_org = string.strip(word_org)
            word_tar = string.strip(word_tar)
            self.map_a[word_org] = word_tar

        for line in file(self.in_fn):
            line = string.strip(line)

            if 'MLF' in line:
                print>> syll_out, line
                print>> word_out2, line
            elif 'lab' in line:
                print>> syll_out, line
                print>> word_out2, line
            elif 'SENT' in line:
                print>> syll_out, line
                print>> word_out2, line
            elif '.pcm' in line:
                print>> syll_out, line
                print>> word_out2, line
            elif line == '.':
                print>> syll_out, line
                print>> word_out2, line
            else:
                line = line.replace('_', '')
                line = line.lower()
                if line in self.map_a:
                    out_word = self.map_a[line]
                    tmp_a = out_word.split()
                    if len(tmp_a) == 1:
                        sylA = self.make_syl_a(out_word)
                        for ss in sylA:
                            print>> syll_out, ss
                    else:
                        for ww in tmp_a:
                            sylA = self.make_syl_a(ww)
                            for ss in sylA:
                                print>> syll_out, ss
                    if out_word[0].isdigit():
                        print>> word_out2, 'dd_%s' % out_word
                    else:
                        print>> word_out2, out_word

                else:
                    sylA = self.make_syl_a(line)
                    for ss in sylA:
                        print>> syll_out, ss
                    if line[0].isdigit():
                        print>> word_out2, 'dd_%s' % line
                    else:
                        print>> word_out2, line

        ssA = self.sym_a.keys()
        ssA.sort()
        for ww in ssA:
            print>> sym_out3, ww

    def do_word_to_syllable(self, prj_name_param):
        self.prj_name = prj_name_param
        ##### 정답 파일을 음절단위로 구분한다.

        self.in_fn = os.path.join(self.RESOURCE_PATH, self.prj_name, self.prj_name + '_dictation.mlf')
        self.in_fn2 = self.RESOURCE_PATH + 'exefiles/split.txt'
        dict_syll_out = file(
            os.path.join(self.RESOURCE_PATH, self.prj_name, self.prj_name + '_dictation.split_syllable.mlf'),
            'w')
        dict_word_out2 = file(os.path.join(self.RESOURCE_PATH, self.prj_name, self.prj_name + '_dictation.word.mlf'),
                              'w')
        dict_sym_out3 = file(os.path.join(self.RESOURCE_PATH, self.prj_name, self.prj_name + '_dictation.sym.txt'), 'w')
        self.sym_a = {}
        self.map_a = {}

        self.word_check(dict_syll_out, dict_word_out2, dict_sym_out3)

        ##### 풀어낸 파일을 음절단위로 구분한다.

        self.in_fn = os.path.join(self.RESOURCE_PATH, self.prj_name, self.prj_name + '_result.mlf')
        self.in_fn2 = self.RESOURCE_PATH + 'exefiles/split.txt'
        result_syll_out = file(
            os.path.join(self.RESOURCE_PATH, self.prj_name, self.prj_name + '_result.split_syllable.mlf'),
            'w')
        result_word_out2 = file(os.path.join(self.RESOURCE_PATH, self.prj_name, self.prj_name + '_result.word.mlf'),
                                'w')
        result_sym_out3 = file(os.path.join(self.RESOURCE_PATH, self.prj_name, self.prj_name + '_result.sym.txt'), 'w')
        self.sym_a = {}
        self.map_a = {}

        self.word_check(result_syll_out, result_word_out2, result_sym_out3)
