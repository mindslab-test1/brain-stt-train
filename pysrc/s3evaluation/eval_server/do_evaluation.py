# -*- coding: utf-8 -*-

import sys
from errno import ENOENT

reload(sys)

import string, re, os
import shutil
from common.config import Config
from maum.brain.stt.train import eval_pb2


class DoEvaluation:
    def __init__(self):
        self.conf = Config()
        self.conf.init('brain-stt-train.conf')
        self.RESOURCE_PATH = self.conf.get('brain-stt.eval.resource.evaluation')

        self.inResult = ''
        self.in_ref = ''
        self.out_recog = ''
        self.out_recog_each = ''
        self.out_hsdi = ''
        self.eval_unit = ''
        self.prj_name = 'tempdist'
        self.out_all_hsdi = ''

    def getProjFilePath(self, target_name):
        return os.path.join(self.RESOURCE_PATH, self.prj_name, self.prj_name + target_name)

    def doEvaluation(self, eval_unit_param, prj_name_param):
        self.prj_name = prj_name_param

        self.out_recog = self.getProjFilePath('_recog.syl')
        self.out_recog_each = self.getProjFilePath('_recog_each.syl')
        self.out_hsdi = self.getProjFilePath('_hsdi.syl')
        self.out_all_hsdi = self.getProjFilePath('_all_hsdi.txt')

        if eval_pb2.EVAL_SYLLABLE_UNIT == eval_unit_param:
            self.eval_unit = 'syl'
            self.inResult = self.getProjFilePath('_result.split_syllable.mlf')
            self.in_ref = self.getProjFilePath('_dictation.split_syllable.mlf')
        elif eval_pb2.EVAL_WORD_UNIT == eval_unit_param:
            self.eval_unit = 'word'
            self.inResult = self.getProjFilePath('_result.word.mlf')
            self.in_ref = self.getProjFilePath('_dictation.word.mlf')
        else:
            self.eval_unit = 'keyword'
            self.inResult = self.getProjFilePath('_result.keyword.mlf')
            self.in_ref = self.getProjFilePath('_dictation.keyword.mlf')

        PRINT_ALL = 0

        out_recog_fd = file(self.out_recog, 'w')
        out_each = file(self.out_recog_each, 'w')
        out_all_hsdi = file(self.out_all_hsdi, 'w') # 단어의 시작과 끝을 기록하는 파일

        word_a = []

        print>> out_each, 'FILE\tCorr\tAcc\tH\tD\tS\tI\tN'

        mlf_a = {}
        for line in file(self.in_ref):
            line = string.strip(line)
            if line == '':
                continue
            if '!MLF!' in line:
                continue

            if '.lab"' in line:
                item_a = line.split('/')
                tmp_w = item_a[1]
                tmp_a = tmp_w.split('.')
                fname = tmp_a[0]
                word_a = []
            elif 'SENT_' in line:
                continue
            elif '.' == line:
                mlf_a[fname] = word_a
            else:
                word_a.append(line)
        print '%d reference loaded' % len(mlf_a)

        result_a = {}
        fname_list = []
        nbest = 0
        for line in file(self.inResult):

            line = string.strip(line)

            if line == '':
                continue
            if '!MLF!' in line:
                continue
            if '<s>' in line or '</s>' in line or 'SENT_' in line:
                continue
            elif '.pcm"' in line or '.raw"' in line or '.wav"' in line or '.MFCC"' in line:
                item_a = line.split('/')
                tmp_w = item_a[-1]
                tmp_a = tmp_w.split('.')
                fname = tmp_a[0]
                word_a = []
                nbest = 1
            elif '.' == line:
                result_a[fname] = word_a
                fname_list.append(fname)
            elif '///' == line:
                nbest += 1
            else:
                if nbest == 1:
                    tmp_a = line.split()
                    if len(tmp_a) == 1:
                        word_a.append(line)
                    elif len(tmp_a) == 4:
                        word_a.append(tmp_a[2])
                    else:
                        print 'format error'
                        sys.exit(1)

        print '%d result loaded' % len(result_a)

        out_result = self.RESOURCE_PATH + self.prj_name + '/result.txt'
        out_ref = self.RESOURCE_PATH + self.prj_name + '/ref.txt'
        out_recog = self.RESOURCE_PATH + self.prj_name + '/recog.txt'
        out_hsdi = self.RESOURCE_PATH + self.prj_name + '/hsdi.txt'

        def pp_text(word):
            tmp_w = re.sub(',', '', word)
            tmp_w = re.sub(']', '', tmp_w)
            tmp_a = tmp_w.split('=')
            nn = int(tmp_a[1])

            return nn

        num_h = 0
        num_d = 0
        num_s = 0
        num_i = 0
        num_n = 0
        num_sent_N = 0
        num_sent_H = 0
        for fname in fname_list:
            if fname not in mlf_a:
                print '%s not defined in reference' % fname
                sys.exit(1)

            ref = mlf_a[fname]
            result = result_a[fname]

            out1 = file(out_result, 'w')
            out2 = file(out_ref, 'w')

            print>> out1, '%s' % string.join(result, ' ')
            print>> out2, '%s' % string.join(ref, ' ')
            out1.close()
            out2.close()

            cmd = self.RESOURCE_PATH + 'exefiles/evaluationTool.exe %s %s %s %s %s' % (
                out_ref, out_result, out_recog, out_hsdi, self.eval_unit)
            os.system(cmd)

            num_sent_N += 1
            lab_sent = ''
            rec_sent = ''

            with open(out_recog, 'r') as out_recog_content:
                for line in out_recog_content:

                    line = string.strip(line)
                    if line == '':
                        continue
                    if 'LAB:' in line:
                        tmp_a = line.split('LAB:')
                        tmp_w = tmp_a[-1]
                        lab_sent = string.strip(tmp_w)
                    if 'REC:' in line:
                        tmp_a = line.split('REC:')
                        tmp_w = tmp_a[-1]
                        rec_sent = string.strip(tmp_w)
                        if lab_sent != rec_sent:
                            print>> out_recog_fd, 'Aligned transcription: %s.lab vs %s.raw' % (fname, fname)
                            print>> out_recog_fd, 'LAB: %s' % lab_sent
                            print>> out_recog_fd, 'REC: %s' % rec_sent
                        else:
                            num_sent_H += 1
                            if PRINT_ALL == 1:
                                print>> out_recog_fd, 'Aligned transcription: %s.lab vs %s.raw' % (fname, fname)
                                print>> out_recog_fd, 'LAB: %s' % lab_sent
                                print>> out_recog_fd, 'REC: %s' % rec_sent

                    elif 'WORD:' in line and 'Corr=' in line:
                        item_a = line.split('[')
                        tmp_w = item_a[-1]
                        item_a = tmp_w.split()

                        each_num_H = pp_text(item_a[0])
                        num_h += each_num_H
                        each_num_D = pp_text(item_a[1])
                        num_d += each_num_D
                        each_num_S = pp_text(item_a[2])
                        num_s += each_num_S
                        each_num_I = pp_text(item_a[3])
                        num_i += each_num_I
                        each_num_N = pp_text(item_a[4])
                        num_n += each_num_N

                        if each_num_N == 0:
                            each_corr = 0.0
                        else:
                            each_corr = (float(each_num_H) / float(each_num_N)) * 100.0
                        if (each_num_S + each_num_D + each_num_H) == 0:
                            each_acc = 0.0
                        else:
                            each_acc = 100. - 100.0 * (
                                float(each_num_S + each_num_D + each_num_I) / float(
                                    each_num_S + each_num_D + each_num_H))

                        print>> out_each, '%s\t%3.2f\t%3.2f\t%d\t%d\t%d\t%d\t%d' % (
                            fname + '.wav', each_corr, each_acc, each_num_H, each_num_D, each_num_S, each_num_I,
                            each_num_N)

            ### for all hsdi : just recode
            '''
            out_all_hsdi.write('\nAligned transcription: '+fname+'.lab vs '+fname+'.raw')
            for line in open(out_hsdi,'r'):
                out_all_hsdi.write(line)
            out_all_hsdi.write('\n')
            '''


        if num_sent_N == 0:
            corr_sent = 0.0
        else:
            corr_sent = (float(num_sent_H) / float(num_sent_N) * 100.0)
        if num_n == 0:
            corr = 0.0
        else:
            corr = (float(num_h) / float(num_n)) * 100.0
        if (num_s + num_d + num_h) == 0:
            acc = 0.0
        else:
            acc = 100. - 100.0 * (float(num_s + num_d + num_i) / float(num_s + num_d + num_h))

        # print '======================== Results Analysis ========================='
        # print '  Ref : %s' %self.in_ref
        # print '  Rec : %s' %self.inResult
        # print '------------------------ Overall Results --------------------------'
        # #print 'SENT: %%Correct=%3.2f [H=%d, S=%d, N=%d]' %(corr_sent,num_sent_H, num_sent_N-num_sent_H, num_sent_N)
        # print '%%Corr=%3.2f, Acc=%3.2f [H=%d, D=%d, S=%d, I=%d, N=%d]' %(corr, acc, num_h, num_d, num_s, num_i, num_n);
        # print '==================================================================='

        try:
            shutil.rmtree(self.RESOURCE_PATH + self.prj_name)
            os.remove(self.RESOURCE_PATH + self.prj_name + '.list')
        except OSError as e:
            if e.errno == ENOENT:
                # 파일이나 디렉토리가 없음!
                print 'No such file or directory to remove'
                pass
            else:
                raise

        return eval_pb2.EvalResult(hit_count=num_h, delete_count=num_d,
                                   switch_count=num_s, insert_count=num_i,
                                   total_count=num_n)
