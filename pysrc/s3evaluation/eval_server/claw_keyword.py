# -*- coding: utf-8 -*-

import sys

reload(sys)


class ClawKeyword:
    def __init__(self):
        pass

    def do_claw_keyword(self):
        input_file = open('keyword.list')
        lines = input_file.readlines()
        input_file.close()

        keyword_list_len = {}
        for line in lines:
            line = line.strip().replace(' ', '')
            if len(line) < 1:
                continue
            if line[0] == '#':
                continue
            keyword_len = len(line)
            if keyword_len not in keyword_list_len:
                keyword_list_len[keyword_len] = [line]
            else:
                keyword_list_len[keyword_len].append(line)

        keyword_list = []
        key_list = keyword_list_len.keys()
        key_list.reverse()
        for key in key_list:
            temp = list(set(keyword_list_len[key]))
            temp.sort()
            for keyword in temp:
                keyword_list.append(keyword)

        input_file = open(sys.argv[1])
        lines = input_file.readlines()
        input_file.close()

        output_file = open(sys.argv[2], 'w')

        temp = ''
        for line in lines:
            line = line.strip()
            if line[0] == '#' or line[0] == '"':
                output_file.write(line + '\n')
            elif line[0] == '.':
                write_keyword_list = {}
                for keyword in keyword_list:
                    while temp.find(keyword) != -1:
                        write_keyword_list[temp.find(keyword)] = keyword
                        temp = temp[:temp.find(keyword)] + '_' * len(keyword) + temp[temp.find(keyword) + len(keyword):]
                key_list = list(set(write_keyword_list.keys()))
                key_list.sort()
                for i in range(len(key_list)):
                    output_file.write(write_keyword_list[key_list[i]] + '\n')
                output_file.write('.\n')
                temp = ''
            else:
                temp += line

        output_file.close()
