# -*- coding: utf-8 -*-

import sys, uuid, os

reload(sys)

from maum.brain.stt.train import eval_pb2
from maum.brain.stt.train import eval_pb2_grpc
from raw_to_word import RawToWord
from word_to_syllable import WordToSyllable
from do_evaluation import DoEvaluation
from common.config import Config

_ONE_DAY_IN_SECONDS = 60 * 60 * 24


class SttEvaluation(eval_pb2_grpc.SttEvalModelResolverServicer):
    # 단어와 의미를 나타내는 것
    # 1. 정답파일(txt, dictation) : 사람이 기입한 정답글
    # 2. ai가 푼파일(result)

    # input으로 필요한것 : 각 pcm마다 정답파일과 ai가 푼파일
    def __init__(self):
        self.conf = Config()
        self.conf.init('brain-stt-train.conf')
        self.RESOURCE_PATH = self.conf.get('brain-stt.eval.resource.evaluation')

        self.inst_raw_to_word = RawToWord()
        self.inst_word_to_syll = WordToSyllable()
        self.inst_do_eval = DoEvaluation()

    def RequestEvaluation(self, request, context):
        print 'RequestEvaluation'

        eval_unit = request.unit_type
        contents_list = []
        for idx, doc in enumerate(request.pairs):
            contents_list.append(doc)

        prj_name = str(uuid.uuid4())

        prj_path = os.path.join(self.RESOURCE_PATH, prj_name)
        if not os.path.exists(prj_path):
            os.makedirs(prj_path)

        self.inst_raw_to_word.do_raw_to_word(contents_list, prj_name)
        self.inst_word_to_syll.do_word_to_syllable(prj_name)

        # if s3evaluation_pb2.EvalUnit.EVAL_SYLLABLE_UNIT != eval_unit:
        #     self.inst_clawKeyword.doClawKeyWord()

        return self.inst_do_eval.doEvaluation(eval_unit, prj_name)
