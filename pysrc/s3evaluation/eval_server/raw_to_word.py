# -*- coding: utf-8 -*-
import string, os
from common.config import Config


class RawToWord:
    def __init__(self):
        self.conf = Config()
        self.conf.init('brain-stt-train.conf')
        self.RESOURCE_PATH = self.conf.get('brain-stt.eval.resource.evaluation')
        self.prj_name = 'tempdist'
        # 단어와 의미를 나타내는 것
        # 1. 정답파일(txt, dictation) : 사람이 기입한 정답글
        # 2. ai가 푼파일(result)

        # input으로 필요한것 : pcm파일 리스트,  각 pcm마다 정답파일,  각 pcm 마다 ai가 푼파일

    def do_raw_to_word(self, contentsList, prj_name_param):
        self.prj_name = prj_name_param
        self.do_raw_to_sentense(contentsList)
        self.do_sentense_to_word(contentsList)

    def make_mlf(self, out, wst_in_file_nm, pcm_file_nm):
        print>> out, pcm_file_nm

        for sent in file(wst_in_file_nm):
            sent = string.strip(sent)

            if sent == '':
                continue
            item_a = sent.split()
            for ww in item_a:
                print>> out, ww
                # print>> out, ww
        print>> out, '.'

    def do_raw_to_sentense(self, contents_list):

        pcm_file_list_out = open(self.RESOURCE_PATH + self.prj_name + ".list", 'w')
        for idx, content in enumerate(contents_list):

            ##### 정답파일 적기 ( 파일이름을 숫자로 시작하게 해야함)
            output_file = open(os.path.join(self.RESOURCE_PATH, self.prj_name, str(idx) + "v" + ".txt"), 'w')
            answer_text_ref = content.answer_contents.replace('\\r', '').replace('\\n', '')

            if answer_text_ref != '':
                print>> pcm_file_list_out, os.path.join(self.prj_name, str(idx) + "v" + ".pcm")
                output_file.write(answer_text_ref.encode('cp949'))
                output_file.write('\n')
            output_file.close()

            ##### ai파일 적기
            output_file = open(os.path.join(self.RESOURCE_PATH, self.prj_name, str(idx) + "v" + ".result"), 'w')

            if content.solve_contents != '':
                output_file.write(content.solve_contents.encode('cp949'))
                output_file.write('\n')

            output_file.close()

        pcm_file_list_out.close()

        curpath = os.getcwd()
        os.chdir(self.RESOURCE_PATH + 'exefiles')

        ##### ai로 풀어낸 파일을 문장단위로 나누기
        os.system('./wst_list.exe ' + self.prj_name + ' result ./rsc')

        ##### 정 답 파일을 문장단위로 나누기
        os.system('./wst_list.exe ' + self.prj_name + ' txt ./rsc')

        os.chdir(curpath)

    def do_sentense_to_word(self, contents_list):

        ##### 풀어낸파일을 단어로 나누기

        word_unit_file_nm = os.path.join(self.RESOURCE_PATH, self.prj_name, self.prj_name + '_result.mlf')
        out = file(word_unit_file_nm, 'w')
        print>> out, '#!MLF!#'

        for idx, content in enumerate(contents_list):
            wst_file_nm = os.path.join(self.RESOURCE_PATH, self.prj_name, str(idx) + 'v' + '.result.wst')
            pcm_file_nm = '"*/' + str(idx) + 'v.pcm"'
            self.make_mlf(out, wst_file_nm, pcm_file_nm)

        out.close()

        ##### 정답파일을 단어로 나누기
        word_unit_file_nm = os.path.join(self.RESOURCE_PATH, self.prj_name, self.prj_name + '_dictation.mlf')
        out = file(word_unit_file_nm, 'w')
        print>> out, '#!MLF!#'

        for idx, content in enumerate(contents_list):
            wst_file_nm = os.path.join(self.RESOURCE_PATH, self.prj_name, str(idx) + 'v' + '.txt.wst')
            pcm_file_nm = '"*/' + str(idx) + 'v.lab"'
            self.make_mlf(out, wst_file_nm, pcm_file_nm)

        out.close()
