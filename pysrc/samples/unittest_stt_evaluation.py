#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import unittest

from do_evaluation import DoEvaluation
from word_to_syllable import WordToSyllable

from common.config import Config
from eval_server.raw_to_word import RawToWord
from maum.brain.stt.train import eval_pb2


class TestUnitTest(unittest.TestCase):
    def setUp(self):

        self.inst_raw_to_word = RawToWord()
        self.inst_word_to_syl = WordToSyllable()
        self.inst_eval = DoEvaluation()
        self.messages = [
            self.make_example_test(
                '행복을 안 드리는 신한카드 김나리입니다 무엇을 도와드릴까요 예 신한카드 저거 마일리지 거 적립 거기에 대해서 한번 물어볼려고 하는데 네 고객님 어떤걸 안내해드릴까요 지금 일월에서 뭐 시월까지 누적된게 마이너스 일육공오공공 원인데 네 그건 뭔 내용인가 물어볼려요 여태까지 할인을 받으셨던거세요 뭐 예를 들어서 포인트 뿐만이 아니고 예를 들어서 뭐 무이자 처리 받으셨다던지 아니면은 뭐 포인트 사용하셨던 금액이라던지 전부 다 토탈로 해서 여태까지 고객님께서 이만큼 혜택을 받아보셨다 이런 부분이십니다 마일리지가 칠육이오 포인트 있는데 네 그거는 어떻게 여는 거에요 일단 칠육이오 포인트면은 칠천육백이십오점이구요 금액으로 따지면은 칠천육백이십오원이거든요 예 저희 신한신용 카드 가지고 있으시다면 예 에스오일이나 현대오일뱅크 가셔가지고 예 이용도 가능하실거구요 대신에 카드대금이 이로 카드결제가 되어야지만 포인트에서 차감도 가능하실거구요 그러면 지금 내가 우리 경찰 저기저 신한카드 네 우리카드는 그 한도액은 얼만가 모르죠 그거는 이 봐드릴게요 고객님 한선봉 고객님 본인 맞으세요 예 네 반갑습니다 혹시 체크카드 말씀하시는거세요 사사구구 체크카드 봐드릴게요 자동 이체 하시는 은행 어떻게 되죠 신용카드 건으로 여쭤보는겁니다 농협농협 네 맞습니다 결제 날짜 몇일이세요 이십이오 가만히 있어봐 잠깐만 오일인가 아 그럼 휴대폰 번호 어떻게 되세요 고객님 가만히 있어봐 오이일삼 어딨냐 시월 칠일이니께 오일로 되어 있으까 시월 칠일 매달 칠일이신데요 네 시월 칠일 네 칠일 맞습니다 해가지구요 일단은 경찰청 카드는 농협 체크카드로 확인되시는데 이거는 고객님께서 농협 잔고가 있어야지만 사용하시는거잖아요 케이엠피 카드는 그럼 없어요 네 그렇죠 그래서 예 죄송합니다 신한카드 김나리 즐거운 하루 되세요',

                '행 행복을 드리는 신한 카드 김나리입니다\
                                    무엇을 도와드릴까요\
                                    이 이 신한 카드 저거 마일리지 거 적립 거기에 대해서 물어 볼려고 하는데 네 고객님 어떤 걸 안내해 드릴까요\
                                    지금 일 월에서 뭐 시 월까지 누적 볼missing 마이너스일 육공 오공공원인데 네 그건 뭔 내용인가 물어 볼 네 일까지 할인을 받으셨던 거세요\
                                    뭐 예를 들어서 포인트 뿐만이 아니고 예를 들어서 뭐 무이자 처리 받으셨다던지 아니면은 뭐 포인트 사용하셨던 금액이라 던지 전부 다 토탈로 해서 여태까지 고객님께서 이만큼 혜택을 받아 보셨다 이런 부분이십니다\
                                    마일리지가 칠 육 이오포인트 있는데 네 그거는 어떻게였나요\
                                    일단 칠 육 이오 포인트면은 칠천육백 이십오 점이구요\
                                    금액으로 따지면은 칠천육백 이십오원이거든요\
                                    예 저희 신한 신용 카드 가지고 있으시다면 예 에스오일이나 현대 오일 뱅크 가셔가지고 예 이용도 가능하실 거구요\
                                    대신에 카드 대금이 이로 카드 결제가 되어야지 만포인트에서 차감도 가능하실 거구요\
                                    그러면 지금 되는 우리 경찰 저기 신한 카드 메일 카드는 그 한도액은 얼만가 모르지 그거는 이 봐드릴게요\
                                    고객님한 선봉 고객님 본인 맞으세요\
                                    예 네 반갑습니다\
                                    혹시 체크 카드 말씀하시는 거세요\
                                    사사구구 체크 카드 봐드릴께요\
                                    자동 이체하시는 은행 어떻게 되신용 카드 건으로 여쭤보는 겁니다\
                                    농협 농협네 맞습니다\
                                    결제 날짜 몇일이세요\
                                    이십일원 잠시만 잠깐만 오일인가 아 그럼 휴대폰 번호 어떻게 되세요\
                                    고객님 잠시만 후유증 등 시 월 칠 일이니 께 오일로 되어 있그럼 주시 월 칠 일네 칠일 매달 칠 일이신데요\
                                    네시 월 칠 일네 칠일 맞습니다\
                                    입력하시구요\
                                    일단은 경찰청 카드는 농협 체크 카드 확인되시는데 이거는 고객님께서 농협 잔고가 있어야지만 사용하시는 거 잖아요\
                                    케이엠 피 카드는 그럼 예를 들어서 그래서 예 죄송합니다\
                                    신한 카드 김나리 즐거운 하루 되세요'),
            self.make_example_test(
                '행복을 드리는 신한카드 김나리입니다 무엇을 도와드릴까요 예 신한카드 저거 마일리지 거 적립 거기에 대해서 한번 물어볼려고 하는데 네 고객님 어떤걸 안내해드릴까요 지금 일월에서 뭐 시월까지 누적된게 마이너스 일육공오공공 원인데 네 그건 뭔 내용인가 물어볼려요 여태까지 할인을 받으셨던거세요 뭐 예를 들어서 포인트 뿐만이 아니고 예를 들어서 뭐 무이자 처리 받으셨다던지 아니면은 뭐 포인트 사용하셨던 금액이라던지 전부 다 토탈로 해서 여태까지 고객님께서 이만큼 혜택을 받아보셨다 이런 부분이십니다 마일리지가 칠육이오 포인트 있는데 네 그거는 어떻게 여는 거에요 일단 칠육이오 포인트면은 칠천육백이십오점이구요 금액으로 따지면은 칠천육백이십오원이거든요 예 저희 신한신용 카드 가지고 있으시다면 예 에스오일이나 현대오일뱅크 가셔가지고 예 이용도 가능하실거구요 대신에 카드대금이 이로 카드결제가 되어야지만 포인트에서 차감도 가능하실거구요 그러면 지금 내가 우리 경찰 저기저 신한카드 네 우리카드는 그 한도액은 얼만가 모르죠 그거는 이 봐드릴게요 고객님 한선봉 고객님 본인 맞으세요 예 네 반갑습니다 혹시 체크카드 말씀하시는거세요 사사구구 체크카드 봐드릴게요 자동 이체 하시는 은행 어떻게 되죠 신용카드 건으로 여쭤보는겁니다 농협농협 네 맞습니다 결제 날짜 몇일이세요 이십이오 가만히 있어봐 잠깐만 오일인가 아 그럼 휴대폰 번호 어떻게 되세요 고객님 가만히 있어봐 오이일삼 어딨냐 시월 칠일이니께 오일로 되어 있으까 시월 칠일 매달 칠일이신데요 네 시월 칠일 네 칠일 맞습니다 해가지구요 일단은 경찰청 카드는 농협 체크카드로 확인되시는데 이거는 고객님께서 농협 잔고가 있어야지만 사용하시는거잖아요 케이엠피 카드는 그럼 없어요 네 그렇죠 그래서 예 죄송합니다 신한카드 김나리 즐거운 하루 되세요',
                '행복 행복을 드리는 신한 카드 김나리입니다\
                                        무엇을 도와드릴까요\
                                        이 이 신한 카드 저거 마일리지 거 적립 거기에 대해서 물어 볼려고 하는데 네 고객님 어떤 걸 안내해 드릴까요\
                                        지금 일 월에서 뭐 시 월까지 누적 missing 마이너스일 육공 오공공원인데 네 그건 뭔 내용인가 물어 볼 네 일까지 할인을 받으셨던 거세요\
                                        뭐 예를 들어서 포인트 뿐만이 아니고 예를 들어서 뭐 무이자 처리 받으셨다던지 아니면은 뭐 포인트 사용하셨던 금액이라 던지 전부 다 토탈로 해서 여태까지 고객님께서 이만큼 혜택을 받아 보셨다 이런 부분이십니다\
                                        마일리지가 칠 육 이오포인트 있는데 네 그거는 어떻게였나요\
                                        일단 칠 육 이오 포인트면은 칠천육백 이십오 점이구요\
                                        금액으로 따지면은 칠천육백 이십오원이거든요\
                                        예 저희 신한 신용 카드 가지고 있으시다면 예 에스오일이나 현대 오일 뱅크 가셔가지고 예 이용도 가능하실 거구요\
                                        대신에 카드 대금이 이로 카드 결제가 되어야지 만포인트에서 차감도 가능하실 거구요\
                                        그러면 지금 되는 우리 경찰 저기 신한 카드 메일 카드는 그 한도액은 얼만가 모르지 그거는 이 봐드릴게요\
                                        고객님한 선봉 고객님 본인 맞으세요\
                                        예 네 반갑습니다\
                                        혹시 체크 카드 말씀하시는 거세요\
                                        사사구구 체크 카드 봐드릴께요\
                                        자동 이체하시는 은행 어떻게 되신용 카드 건으로 여쭤보는 겁니다\
                                        농협 농협네 맞습니다\
                                        결제 날짜 몇일이세요\
                                        이십일원 잠시만 잠깐만 오일인가 아 그럼 휴대폰 번호 어떻게 되세요\
                                        고객님 잠시만 후유증 등 시 월 칠 일이니 께 오일로 되어 있그럼 주시 월 칠 일네 칠일 매달 칠 일이신데요\
                                        네시 월 칠 일네 칠일 맞습니다\
                                        입력하시구요\
                                        일단은 경찰청 카드는 농협 체크 카드 확인되시는데 이거는 고객님께서 농협 잔고가 있어야지만 사용하시는 거 잖아요\
                                        케이엠 피 카드는 그럼 예를 들어서 그래서 예 죄송합니다\
                                        신한 카드 김나리 즐거운 하루 되세요'),
            self.make_example_test("Third messagef", "Third messageg"),
            self.make_example_test("아졸려", "아쫄려"),
            self.make_example_test("Fifth message", "Fifth message")
        ]

        self.conf = Config()
        self.conf.init('brain-stt-train.conf')
        self.RESOURCE_PATH = self.conf.get('brain-stt.eval.resource.evaluation')
        self.prj_name = 'tempdist'
        prj_path = os.path.join(self.RESOURCE_PATH, self.prj_name)
        if not os.path.exists(prj_path):
            os.makedirs(prj_path)

    def make_example_test(self, solve_contents, answer_contents):
        return eval_pb2.EvalPair(
            solve_contents=solve_contents, answer_contents=answer_contents)

    def test_all_process(self):
        print 'test_all_process'

        self.inst_raw_to_word.do_raw_to_word(self.messages, self.prj_name)

        self.inst_word_to_syl.do_word_to_syllable(self.prj_name)

        result = self.inst_eval.doEvaluation(eval_pb2.EVAL_SYLLABLE_UNIT, self.prj_name)

        self.assertTrue(result.hit_count == 1267)
        self.assertTrue(result.total_count == 1355)

        # def test_doEvaluation(self):
        #     print 'test_doEvaluation'
        #     result = self.inst_eval.doEvaluation()
        #     self.assertTrue(result.hit_count == '626')
        #     self.assertTrue(result.total_count == '669')


if __name__ == '__main__':
    unittest.main()
