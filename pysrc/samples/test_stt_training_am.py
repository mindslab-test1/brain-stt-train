#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os
import sys
import grpc
import time

from google.protobuf import json_format, empty_pb2
from common.config import Config

from maum.common import lang_pb2
from maum.brain.stt.train import s3train_pb2
from maum.brain.stt.train import s3train_pb2_grpc


exe_path = os.path.realpath(sys.argv[0])
bin_path = os.path.dirname(exe_path)
lib_path = os.path.realpath(bin_path + '/../lib/python')
sys.path.append(lib_path)


class SttTrainerClient:
    conf = Config()
    stub = None
    key = None

    def __init__(self):
        remote = '127.0.0.1:' + conf.get('brain-stt.s3traind.front.port')
        print remote
        channel = grpc.insecure_channel(remote)
        self.stub = s3train_pb2_grpc.SttTrainerStub(channel)

    def open(self, model):
        key = self.stub.Open(model)
        self.key = key
        json_ret = json_format.MessageToJson(key, True)
        print json_ret

    def get_progress(self):
        stat = self.stub.GetProgress(self.key)
        json_ret = json_format.MessageToJson(stat, True)
        print json_ret
        return stat

    def get_all_progress(self):
        stat = self.stub.GetAllProgress(empty_pb2.Empty())
        json_ret = json_format.MessageToJson(stat, True)
        print json_ret
        return stat

    def get_binary(self):
        tar = self.stub.GetBinary(self.key)
        return tar

    def close(self):
        stat = self.stub.Close(self.key)
        json_ret = json_format.MessageToJson(stat, True)
        print json_ret
        return stat


def test_s3trainer(model):
    client = SttTrainerClient()
    client.open(model)

    time.sleep(5)
    while True:
        proc = client.get_progress()
        if proc.result == s3train_pb2.training:
            # 학습중일 때 5초마다 체크
            time.sleep(5)
        elif proc.result == s3train_pb2.success:
            # 학습 완료
            if model.lang == lang_pb2.eng:
                lang = 'eng'
            else:
                lang = 'kor'
            with open('/home/minds/user/wontop/' + model.model + '-' + lang + '-' + str(model.sample_rate) + '.sttimage.tar.gz', 'wb') as f:
                for tar in client.get_binary():
                    f.write(tar.tar)

            break
        else:
            break
    client.close()

    print 'done'


def make_kor_model():
    model = s3train_pb2.SttModel()
    model.model = 'test'
    model.train_model_type = s3train_pb2.TRAIN_MODEL_AM
    model.deep_learning_type = s3train_pb2.DEEP_LEARNING_DNN
    model.lang = lang_pb2.kor

    model.sample_rate = 16000

    audio_file = model.audio_files.add()
    audio_file.filename = '/home/minds/maum/run/storage/mlt/temp/sinhan.wav'
    audio_file.format = 'wav'
    audio_file.transcript = """행복을 드리는 신한카드 상담원 이진입니다 고갬 무엇을 도와드릴까요 예 안녕하세요 저 빅플러스카드 쓰고 있는데요 네 예 그 지에스칼텍스에서 저기 어 주유 하면 적립되는거요 네네
예 근데 구월 제가 얼마전 주유해보니까 구월부터 포인트가 전혀 적립 안되있더라구요 아 네 적립이 안되어있으신단 부분이신데요 제가 확인을 도와드릴게요 고객님 네 네 최보연고객님 본인이시구요 예
아 반갑습니다 결제계좌는 어느은행이세요 고객님 케이비입니다 네 결제하시는날짜 매달 며칠이세요 십 오일입니다 네 소중한정보확인감사드립니다 말씀주셨던 포인트적립부분 확인 해보겠는데요
잠시만기다려주세요 네 네 기다려주셔서 감사합니다 말씀주신 그 카드로해서 지에스칼텍스 포인트가 적립이되시기 위해서는 전월 일일부터 말일까지의 실적부분이 좀 충족이 되셔야지만 이번달에
예예 주유하시는 부분에 대해서 리터당 적립을 받아보실수 있는데요 현재 소지하고 계신 국방 복지카드라든가 케이티쿡센터카드로는 전월실적부분이 주유 금액을 제외한금액이 일일부터 말일까지
이십만원이상이 되셔야되요 네 주유 그 할인 할인도 포함아닌가요 네 고객님 주유하신금액은 아예 제외가 되시구 다른쪽으로 사용되신 금액이 일시불 할부건이 이십만원 이상이셔야지만 이번달에 지에스칼텍스
에서 주유하실때 적립을 받아보실수 있는 부분이세요 예 근데 제가 그 조회해보시면 알겠지만 매달 한 사십몇만원씩 해서 내고 있는데요 네 고객님께서결제하시는 금액 기준이 아니시구요 실적부분은
전월 일일부터 말일까지기준이시구요 네 예 최근에지금 십일월달까지 지금 쿡세트카드로서 조회를해보자면요 주유업종을 제외하시고는 금액부분이 이십만원이 채 안되시는 부분으로는 확인되셨어요
아 그 할인은요 그 할인 그까 할인이 아니라 그 잠깐만요 어 매달 할부금액 있었을텐데 이십만원씩 할부금액이요 할부 예예 사용건은 그 처음 사용하신 한달에 실적이 다 포함이 되세요 고객님께서
할부를 나눠논 나눠서 결제는 하시지만  그부분 실적이 달달이 들어가는게 아니구요 한꺼번에 사용하신 날짜에 그 달에 포함이 되시는거에요 아 첫 달에 그럼요  네네 맞습니다 이게 언제부터 이렇게 바꼈나요
빅플러스포인트가 적립되시는 그 내용 부분은 작년 시월달부터 저희가 변경이되었습니다 아 그래요 네네 그믄 음 그래요 그면 이게 이거와 비슷한 주유할때 혜택보는 이거 주유 비슷한 카드가 뭐있나요
어 주유하실때 그러시면 이부분은 인제 그 지에스칼텍스빅플러스 포인트로 적립을 받으시는 카드가 이렇게 있구요 이 카드 외에 전월실적 관계 없이 저희가 포인트 지급 해드리는 그 알피엠 카드라고는 있어요
예 네 알피엠카드는 말씀드린것처럼 전월 뭐 실적 상관없이 총 그달에 주유하시는 삼십만원의 주유금액 까지는 리터당 백원씩 저희가 적 포인트로 적립해드리구요 네 횟수로는 월 사회까지 금액은 말씀드렸던것처럼
삼십만원의 주유금액까지  적립해드리구 이부분 카드의 연회비부분은 조금 지금의 이 현재 카드와는 조금 더 나가시는 부분으로 국내전용으로 하시게되시면은 이만칠천원 확인되시구요 네 전월실적부분이 없는대신에
포인트지급은 저희가 더 많이 해드리는 카드로는 있으세요 어 그렇군요 일단은 무슨말씀인지 알겠습니다 카드는 어 예 하튼 그 다시 정리를 해주시면 할부빼고 주유량도 빼고 나머지는 다된다구요
네네 주유 지에스주유소에서 고객님께서 적립 받기위해서는요 주유하신금액은 제외하고 기준은 전월 일일부터 말일까지 일시불 할부실적이 이십만원이상 되셨을때  이번달에 지에스에서 주유하신금액
리터당 적립받으실수 있는 부분이세요 음 알겠습니다 그 이거 저기 리터당 얼마씩 적립이 되나요 다시한번 확인해드릴게요 예 리터당 팔시건 팔십원적립확인되십니다 고객님 팔십원은 그냥 고정이에요 뭐 적립시스
그까카드실적에 따라 이런게 아니구요 네네 고객님 전월실적만 충족이 되신다면 이번달에 주유하신 금액에 지에스 칼텍스 주유하신금액에 리터당 팔십원 적립이시구요 이제 다만 이부분도 좀 적립제한은 있는데요
일회주유시 십오만원까지에 사용금액에 대해서만 적립되시구 월 주유금액 삼십만원까지만 적립되시는부분도 참고부탁드리구요 별도로 포인트 유효기간도 오년 있으신걸로 확인되십니다 네 그 작년
시월에 바꼈다는 내용에 대해서 인지를 못하고 있었어요 아 그러세요 네 저희가 서비스가 변경이 되시거나 하실때에는 그 서비스 변경되시기 그 다섯달을 내지 여섯달 전부터는 명세서  부분을 통해서 고객님께
안내를 해드렸던 부분이신데 이부분 못받아보셨나요 아 예 알겠습니다 무슨말인지 알겠습니다 감사합니다 네 감사합니다 저는 상담원 이미진이였구요 오늘하루행복하세요 예 네 """

    model.callback_url = 'http://10.122.64.148:3000/api/Histories/71/update?key='

    return model


if __name__ == '__main__':
    conf = Config()
    conf.init('brain-stt-train.conf')

    test_s3trainer(make_kor_model())
