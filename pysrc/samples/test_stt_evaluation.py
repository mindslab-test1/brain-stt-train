#!/usr/bin/env python
# -*- coding: utf-8 -*-

import grpc
from common.config import Config
from maum.brain.stt.train import eval_pb2 as eval
from maum.brain.stt.train import eval_pb2_grpc


class SttEvaluationTestWithServer():
    def __init__(self):
        self.messages = [
            self.make_example_test("Third messagef", "Third messageg"),
            self.make_example_test("아졸려", "아쫄려"),
            self.make_example_test("Fifth message", "Fifth message")
        ]

    def make_example_test(self, solve_contents, answer_contents):
        return eval.EvalPair(solve_contents=solve_contents, answer_contents=answer_contents)

    def stt_eval_server_test(self):
        print 'test_stt_eval_server'
        conf = Config()
        conf.init('brain-stt-train.conf')
        channel = grpc.insecure_channel('localhost:' + conf.get('brain-stt.evald.front.port'))
        stub = eval_pb2_grpc.SttEvalModelResolverStub(channel)

        response = stub.RequestEvaluation(eval.EvalRequest(pairs=self.messages, unit_type=eval.EVAL_SYLLABLE_UNIT))
        print("Received message hit_count : " + str(response.hit_count) + " total_count : " + str(response.total_count))


if __name__ == '__main__':
    instance = SttEvaluationTestWithServer()
    instance.stt_eval_server_test()
