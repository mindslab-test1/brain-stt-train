#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os
import sys

import grpc
import time

exe_path = os.path.realpath(sys.argv[0])
bin_path = os.path.dirname(exe_path)
lib_path = os.getenv('MAUM_ROOT') + '/lib/python'
# lib_path = os.path.realpath(bin_path + '/../lib/python')
sys.path.append(lib_path)

from common.config import Config
from maum.brain.stt import stt_pb2
from maum.common import lang_pb2
from maum.common import types_pb2
import time
import getopt

def usage():
    print '%s [-r sample_rate] [-m model] filename' % sys.argv[0]

class SttClient:
    conf = Config()
    real_stub = None
    resolver_stub = None

    def __init__(self):
        remote = '127.0.0.1:' + conf.get('brain-stt.sttd.front.port')
        print remote
        channel = grpc.insecure_channel(remote)
        self.resolver_stub = stt_pb2.SttModelResolverStub(channel)

    def get_servers(self, _name, _lang, _sample_rate):
        """Find & Connect servers"""
        # Define model
        model = stt_pb2.Model()
        if _lang == 'eng':
            model.lang = lang_pb2.eng
        else:
            model.lang = lang_pb2.kor

        model.model = _name
        model.sample_rate = _sample_rate

        try:
            # Find Server
            self.server_status = self.resolver_stub.Find(model)
        except:
            print _name + '-' + _lang + '-' + str(_sample_rate) + ' model cannot found'
            return False

        # Remote STT service
        # channel = grpc.insecure_channel(server_status.server_address)
        # self.real_stub = stt_pb2.SttRealServiceStub(channel)

        # Get STT server status
        wait = 0
        while not self.get_stt_status(model):
            print 'Wait for server ready...'
            time.sleep(0.5)
            wait += 1
            if wait > 20:
                return False
            continue

        return True

    def get_stt_status(self, model):
        """Return STT server status"""
        try:
            # Remote STT service
            channel = grpc.insecure_channel(self.server_status.server_address)
            self.real_stub = stt_pb2.SttRealServiceStub(channel)

            status = self.real_stub.Ping(model)
            print 'Model : ', status.model
            print 'Sample Rate : ', status.sample_rate
            print 'Lang : ', status.lang
            print 'Running : ', status.running
            print 'Server Address : ', status.server_address
            print 'Invoked by : ', status.invoked_by
            return status.running
        except:
            return False

    def simple_recognize(self, audio_file):
        """Speech to Text function"""
        result = self.real_stub.SimpleRecognize(self.bytes_from_file(audio_file))

        print 'RESULT : ', result.txt

    def bytes_from_file(self, filename, chunksize=10000):
        with open(filename, "rb") as f:
            while True:
                chunk = f.read(chunksize)
                if chunk:
                    speech = stt_pb2.Speech()
                    speech.bin = chunk
                    yield speech
                else:
                    break

    def bytes_from_file2(self, filename, chunksize=1024*1024):
        with open(filename, "rb") as f:
            while True:
                chunk = f.read(chunksize)
                if chunk:
                    part = types_pb2.FilePart()
                    part.part = chunk
                    yield part
                else:
                    break

    def set_model(self, filename):
        metadata={(b'in.lang', b'kor'), (b'in.model', 'weather'), (b'in.samplerate', '8000') }
        result = self.resolver_stub.SetModel(self.bytes_from_file2(filename), metadata=metadata)
        print 'RESULT' ,result.lang
        print 'RESULT' ,result.model
        print 'RESULT' ,result.sample_rate
        print 'RESULT' ,result.result
        print 'RESULT' ,result.error

    def delete_model(self, name, lang, sample_rate):
        model = stt_pb2.Model()
        if lang == 'eng':
            model.lang = lang_pb2.eng
        else:
            model.lang = lang_pb2.kor

        model.model = name
        model.sample_rate = sample_rate
        status = self.resolver_stub.DeleteModel(model)
        print 'Model : ', status.model
        print 'Sample Rate : ', status.sample_rate
        print 'Lang : ', status.lang
        print 'Running : ', status.running
        print 'Server Address : ', status.server_address
        print 'Invoked by : ', status.invoked_by
        return status.running


def test_recognize(stt_client, sample_rate, model, filename):
    if stt_client.get_servers(_name=model, _lang='kor', _sample_rate=sample_rate):
        stt_client.simple_recognize(filename)
    else:
        print 'STT Service unavailable'

def test_recognize_8k(stt_client):
    if stt_client.get_servers(_name='baseline', _lang='kor', _sample_rate=8000):
        stt_client.simple_recognize('./8k.pcm')
    else:
        print 'STT Service unavailable'

def test_recognize_16k(stt_client):
    if stt_client.get_servers(_name='baseline', _lang='kor', _sample_rate=16000):
        stt_client.simple_recognize('./16k.pcm')
    else:
        print 'STT Service unavailable'

def test_set_model(stt_client, f):
    stt_client.set_model(f)

def test_delete_model(stt_client):
    stt_client.delete_model(name='weather', lang='kor', sample_rate=8000)

if __name__ == '__main__':
    conf = Config()
    conf.init('brain-stt-train.conf')

    sample_rate = 8000
    model = 'baseline'

    try:
        opts, args = getopt.getopt(sys.argv[1:], 'r:m:', [])
        for option, value in opts:
            if option == '-r':
                sample_rate = int(value)
            elif option == '-m':
                model = value

        if len(args) < 1:
            usage()
            sys.exit(2)
        filename = args[0]

    except getopt.GetoptError as err:
        # print help information and exit:
        print str(err)  # will print something like "option -a not recognized"
        usage()
        sys.exit(2)

    stt_client = SttClient()
    test_recognize(stt_client, sample_rate, model, filename)
    # test_recognize_8k(stt_client)
    # test_recognize_16k(stt_client)
    f = os.path.join(os.environ['HOME'], 'weather-kor-8000.tar.gz')
    if os.path.isfile(f):
      test_set_model(stt_client)
      test_delete_model(stt_client)

