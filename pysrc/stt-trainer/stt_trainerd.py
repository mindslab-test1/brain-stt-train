#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os
import sys

exe_path = os.path.realpath(sys.argv[0])
bin_path = os.path.dirname(exe_path)
lib_path = os.path.realpath(bin_path + '/../lib/python')
sys.path.append(lib_path)

from str_trainer.serve import serve
if __name__ == '__main__':
    serve()
