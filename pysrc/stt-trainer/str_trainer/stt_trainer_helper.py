# -*- coding:utf-8 -*-
import fcntl
import os
import sys
import logger
import time

from google.protobuf import timestamp_pb2

from common.config import Config
from maum.common import lang_pb2
from maum.brain.stt.train import s3train_pb2 as stt_tr


class TrainerHelper:
    conf = Config()
    ws_dir = ''
    proc_name_ = os.path.basename(sys.argv[0]);
    log = logger.Create_Logger(os.path.splitext(proc_name_)[0], 'debug')

    def __init__(self, uuid, stt_model):
        # Train Model Type에 따라 분기
        if stt_model.train_model_type == stt_tr.TRAIN_MODEL_AM:
            self.ws_dir = self.conf.get('brain-stt.s3traind.am.workspace.dir')
        elif stt_model.train_model_type == stt_tr.TRAIN_MODEL_LM:
            self.ws_dir = self.conf.get('brain-stt.s3traind.lm.workspace.dir')
        elif stt_model.train_model_type == stt_tr.TRAIN_MODEL_ALL:
            self.ws_dir = self.conf.get('brain-stt.s3traind.workspace.dir')
        else:
            self.log.critical("HELPER:: __init__ Invalid Model !!!")
            return

        self.stt_model   = stt_model
        self.uuid        = uuid
        self.train_group = self.stt_model.model
        self.sample_rate = self.stt_model.sample_rate
        self.lang_code   = self.stt_model.lang
        self.model_file  = os.path.join(self.ws_dir, str(self.uuid) + '.model')
        self.proc_file   = os.path.join(self.ws_dir, str(self.uuid) + '.proc')
        if not os.path.isdir(self.ws_dir):
            os.makedirs(self.ws_dir)

        self.log.critical("*" * 60)
        self.log.critical("HELPER Init")
        self.log.critical("*" * 60)
        self.log.critical("WS_DIR      : %s", self.ws_dir)
        self.log.critical("UUID        : %s", self.uuid)
        self.log.critical("TRAIN_GROUP : %s", self.train_group)
        self.log.critical("LANG        : %s", lang_pb2.LangCode.Name(self.lang_code))
        self.log.critical("SAMPLE_RATE : %s", self.sample_rate)
        self.log.critical("*" * 60)

    def get_parent_endpoint(self):
        return '127.0.0.1:' + self.conf.get('brain-stt.trained.front.port')

    def workspace_path(self):
        p = os.path.join(self.ws_dir, str(self.uuid))
        p += '/'
        if not os.path.isdir(p):
            os.makedirs(p)
        return p

    def get_model_file(self):
        return self.model_file

    def get_proc_file(self):
        return self.proc_file

    def load_model(self):
        try:
            f = open(self.model_file, 'rb')
            stt_model = stt_tr.SttModel()
            stt_model.ParseFromString(f.read())
            f.close()
            return stt_model
        except IOError, e:
            print 'open failed', self.model_file, e.errno, e.strerror
            return None

    def save_model(self, model):
        f = open(self.model_file, 'wb')
        f.write(model.SerializeToString())
        f.close()

    def load_proc(self):
        try:
            f = open(self.proc_file, 'rb')
            fcntl.lockf(f.fileno(), fcntl.LOCK_SH)
            proc = stt_tr.TrainStatus()

            proc.ParseFromString(f.read())
            fcntl.lockf(f.fileno(), fcntl.LOCK_UN)
            f.close()
            return proc
        except IOError, e:
            print 'open failed', self.proc_file, e.errno, e.strerror
            return None

    def save_proc(self, proc):
        # update elapsed time
        now = time.time()
        seconds = int(now)
        nanos = int((now - seconds) * 10 ** 9)
        end = timestamp_pb2.Timestamp(seconds=seconds, nanos=nanos)
        proc.elapsed.seconds = end.seconds - proc.started.seconds
        proc.elapsed.nanos = end.nanos - proc.started.nanos
        if proc.elapsed.seconds > 0 and proc.elapsed.nanos < 0:
            proc.elapsed.seconds -= 1
            proc.elapsed.nanos += 1000000000

        f = open(self.proc_file, 'wb')
        fcntl.lockf(f.fileno(), fcntl.LOCK_EX)
        f.write(proc.SerializeToString())
        fcntl.lockf(f.fileno(), fcntl.LOCK_UN)
        f.close()

    def remove_proc(self):
        if os.path.exists(self.proc_file):
            f = open(self.proc_file, 'wb')
            fcntl.lockf(f.fileno(), fcntl.LOCK_EX)
            fcntl.lockf(f.fileno(), fcntl.LOCK_UN)
            f.close()
            os.remove(self.proc_file)

    def get_train_group_name(self):
        train_group_name = self.train_group
        train_group_name += '-'
        train_group_name += lang_pb2.LangCode.Name(self.lang_code)
        train_group_name += '-'
        train_group_name += str(self.sample_rate)
        return train_group_name

    def get_tar_filename(self):
        return self.get_train_group_name() + '.sttimage.tar.gz'

    def get_tar_file(self):
        return os.path.join(self.ws_dir, self.get_tar_filename())

    def get_tar_work_dir(self):
        return os.path.join(self.ws_dir, self.get_train_group_name())

