#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os
import sys
import time
import logger

import grpc
from concurrent import futures

exe_path = os.path.realpath(sys.argv[0])
bin_path = os.path.dirname(exe_path)
lib_path = os.path.realpath(bin_path + '/../lib/python')
sys.path.append(lib_path)

from maum.brain.stt.train import s3train_pb2
from maum.brain.stt.train import s3train_pb2_grpc
from stt_trainer_server import SttTrainer
from common.config import Config

_ONE_DAY_IN_SECONDS = 60 * 60 * 24

def serve():
    proc_name_ = os.path.basename(sys.argv[0]);
    log = logger.Create_Logger(os.path.splitext(proc_name_)[0], 'debug')

    log.critical('');
    log.critical("*" * 80);
    log.critical('*%30s : %s', "START", proc_name_);
    log.critical("*" * 80);
    log.critical('stt-traind initializing...');

    conf = Config()
    conf.init('brain-stt-train.conf')
    data = [
        ('grpc.max_connection_idle_ms', int(conf.get("brain-stt.s3traind.front.timeout"))),
        ('grpc.max_connection_age_ms', int(conf.get("brain-stt.s3traind.front.timeout")))
    ]

    svc = SttTrainer()
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10), options=data)
    s3train_pb2_grpc.add_SttTrainerServicer_to_server(svc, server)

    port = conf.get('brain-stt.s3traind.front.port')
    server.add_insecure_port('[::]:' + port)

    log.critical('stt-traind server starting at 0.0.0.0:' + port);
    server.start()
    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)

    except KeyboardInterrupt:
        server.stop(0)
        # Delete logger
        for handler in log.handlers:
            handler.close()
            log.removeHandler(handler)
