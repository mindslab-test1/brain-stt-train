#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os
import sys
import shutil
import syslog

from str_trainer import logger
from common.config import Config
from maum.common import lang_pb2
from maum.brain.stt.train import s3train_pb2 as sttr


reload(sys)
sys.setdefaultencoding('utf-8')

exe_path = os.path.realpath(sys.argv[0])
bin_path = os.path.dirname(exe_path)
lib_path = os.path.realpath(bin_path + '/../lib/python')
sys.path.append(lib_path)

class KoreanSttTrainer:

    def __init__(self, stt_model, helper, proc, log):
        self.stt_model = stt_model
        self.helper = helper
        self.proc = proc
        self.log = log

        # Config Init
        self.conf = Config()
        self.conf.init('brain-stt-train.conf')

        # Train Model Type에 따라 분기
        if self.stt_model.train_model_type == sttr.TRAIN_MODEL_AM:
            self.stt_ws = self.conf.get('brain-stt.s3traind.am.workspace.dir')
        elif self.stt_model.train_model_type == sttr.TRAIN_MODEL_LM:
            self.stt_ws = self.conf.get('brain-stt.s3traind.lm.workspace.dir')
        elif self.stt_model.train_model_type == sttr.TRAIN_MODEL_ALL:
            self.stt_ws = self.conf.get('brain-stt.s3traind.workspace.dir')
        else:
            self.log.critical("SttKor::init() Invalid Model !!!")

        self.train_home = self.conf.get('brain-stt.s3traind.kor.train.dir')
        self.stt_train_data = self.conf.get('brain-stt.trained.root')

    def do_training(self):
        self.log.info("SttKor::do_training()")
        self.proc.step = sttr.TSTEP_START
        self.proc.maximum = 35
        self.proc.value = 0

        self.helper.save_proc(self.proc)

        # Train Model Type에 따라 분기
        if self.stt_model.train_model_type == sttr.TRAIN_MODEL_AM:
            self.log.info("SttKor::do_training() AM")
            return self.am_train()
        elif self.stt_model.train_model_type == sttr.TRAIN_MODEL_LM:
            self.log.info("SttKor::do_training() LM")
            return self.lm_train()
        elif self.stt_model.train_model_type == sttr.TRAIN_MODEL_ALL:
            self.log.info("SttKor::do_training() ALL")
            return self.train_all()
        else:
            self.log.critical("SttKor::do_training() Invalid Model !!!")
            return False

    def train_all(self):
        # AM Train
        ret = self.am_train()
        if not ret:
            self.log.critical("SttKor::train_all() am_train() fail !!!")
            return False

        # LM Train
        ret = self.lm_train()
        if not ret:
            self.log.critical("SttKor::train_all() lm_train() fail !!!")
            return False

        return True


    def am_train(self):
        # step 1
        ret = self.am_make_group()
        if not ret:
            self.log.critical("SttKor::am_train() am_make_group() fail !!!")
            return False

        # step 2
        ret = self.am_make_list()
        if not ret:
            self.log.critical("SttKor::am_train() am_make_list() fail !!!")
            return False

        # step 3
        # AM 학습
        self.log.info("SttKor::am_train() - STEP3 : AM Learning START -")

        self.proc.step = sttr.TSTEP_RUN_AM
        self.helper.save_proc(self.proc)
        ret = self.am_align()
        if not ret:
            self.log.critical("SttKor::am_train() am_align() fail !!!")
            return False

        ret = self.am_adapt()
        if not ret:
            self.log.critical("SttKor::am_train() am_adapt() fail !!!")
            return False

        ret = self.am_copy_image()
        if not ret:
            self.log.critical("SttKor::am_train() am_copy_image() fail !!!")
            return False

        self.proc.value += 5
        self.helper.save_proc(self.proc)
        self.log.info("SttKor::am_train() - STEP3 : AM Learning FINISH -")

        # step 4
        ret = self.am_archive_result()
        if not ret:
            self.log.critical("SttKor::am_train() am_archive_result() fail !!!")
            return False

        return True

    # step 1
    # Transform wav to pcm
    def am_make_group(self):
        self.log.info("SttKor::am_make_group() - STEP1 : START -")

        self.proc.step = sttr.TSTEP_WAV_TO_PCM
        self.helper.save_proc(self.proc)

        # maum/workspace/stt-training-am 밑에 작업Model 디렉토리 재구성
        path = os.path.join(self.stt_ws, self.stt_model.model)
        if not os.path.exists(path):
            os.makedirs(path)
        else:
            shutil.rmtree(path)
            os.makedirs(path)

        pcm_list = open(os.path.join(self.stt_ws, self.stt_model.model + '.list'), 'w')
        pcm_list_all_path = open(os.path.join(self.stt_ws, self.stt_model.model + '.all.list'), 'w')

        for file in self.stt_model.audio_files:
            file_hash = file.filename.split('/')[-1]
            file_hash = os.path.splitext(file_hash)[0]
            self.log.debug('SttKor::am_make_group() file_hase : ' + file_hash)

            self.log.debug('cmd> cp ' + file.filename + ' ' + os.path.join(path, file_hash + '.' + file.format))
            os.system('cp ' + file.filename + ' ' + os.path.join(path, file_hash + '.' + file.format))

            self.log.debug('cmd> sox -t wav ' + os.path.join(path, file_hash + '.' + file.format) 
                + ' -r 8000 -b 16 -t raw ' + os.path.join( path, file_hash + '.pcm'))
            os.system('sox -t wav ' + os.path.join(path, file_hash + '.' + file.format) 
                + ' -r 8000 -b 16 -t raw ' + os.path.join( path, file_hash + '.pcm'))

            # create transcript file ( MUST EUC-KR )
            f = open(os.path.join(path, file_hash + '.txt'), 'wb')
            f.write(file.transcript.encode('euc-kr'))
            f.close()

            # enroll pcm file to pcm_list
            pcm_list.write(self.stt_model.model + '/' + file_hash + '.pcm' + '\n')
            pcm_list_all_path.write(os.path.join(path + '/' + file_hash + '.pcm' + '\n'))

        pcm_list.close()
        pcm_list_all_path.close()

        self.proc.value += 5
        self.helper.save_proc(self.proc)

        self.log.info("SttKor::am_make_group() - STEP1 : FINISH -")

        return True

    # step 2
    # make domain all list
    def am_make_list(self):
        self.log.info("SttKor::am_make_list() - STEP2 : START -")

        self.proc.step = sttr.TSTEP_MAKE_LIST
        self.helper.save_proc(self.proc)

        all_list = os.path.join(self.stt_ws, self.stt_model.model + '.all.list')

        self.log.debug('cmd> cp ' + all_list + ' ' + os.path.join(self.train_home, 'DNN_Adapt_Env/align_env/'))
        os.system('cp ' + all_list + ' ' + os.path.join(self.train_home, 'DNN_Adapt_Env/align_env/'))

        self.proc.value += 5
        self.helper.save_proc(self.proc)

        self.log.info("SttKor::am_make_list() - STEP2 : FINISH -")

        return True

    def am_align(self):
        # do_all.pl
        self.log.info("SttKor::am_do_all() - STEP3-1 : do_all.pl START -")
        os.chdir(os.path.join(self.train_home, 'DNN_Adapt_Env/align_env/'))
        os.system(os.path.join(self.train_home, 'DNN_Adapt_Env/align_env/do_all.pl') + ' ' + self.stt_model.model + '.all.list')

        self.proc.value += 2
        self.helper.save_proc(self.proc)

        self.log.info("SttKor::am_do_all() - STEP3-1 : do_all.pl FINISH -")
        return True

    def am_adapt(self):
        self.log.info("SttKor::am_run_total() - STEP3-2 : run.total.sh START -")
        os.chdir(os.path.join(self.train_home, 'DNN_Adapt_Env/adapt_env/'))
        os.system(os.path.join(self.train_home, 'DNN_Adapt_Env/adapt_env/run.total.sh') + ' ' + self.stt_model.model)
        
        self.proc.value += 2
        self.helper.save_proc(self.proc)
        self.log.info("SttKor::am_run_total() - STEP3-2 : run.total.sh FINISH -")
        return True

    def am_copy_image(self):
        self.log.info("SttKor::am_copy_image() - STEP3-3 : START -")
        # cp
        result = os.path.join(self.stt_ws, self.helper.get_train_group_name())
        if not os.path.exists(result):
            os.makedirs(result)

        os.system('cp -r ' + os.path.join(self.train_home, 'dnn_image_out/final.dnn.adapt.0.0001.avg.bin') 
            + ' ' + os.path.join(result, 'dnn.bin'))
        os.system('cp -r ' + os.path.join(self.train_home, 'dnn_image_out/final.dnn.prior.bin.adapted') 
            + ' ' + os.path.join(result, 'dnn.prior.bin'))

        self.proc.value += 2
        self.helper.save_proc(self.proc)

        self.log.info("SttKor::am_copy_image() - STEP3-3 : FINISH -")

        return True

    # result tar 압축
    def am_archive_result(self):
        self.log.info("SttKor::am_archive_result() - START -")
        self.proc.step = sttr.TSTEP_ARCHIVE
        self.helper.save_proc(self.proc)

        os.chdir(self.stt_ws)
        os.system('tar -zcvf ' + self.helper.get_tar_filename() + ' ' + self.helper.get_train_group_name())

        train_data_dir = os.path.join(self.stt_train_data, self.helper.get_train_group_name())
        if not os.path.exists(train_data_dir):
            os.makedirs(train_data_dir)

        os.system('cp -r ' + self.helper.get_tar_work_dir() + '\/* ' + train_data_dir)

        self.proc.step = sttr.TSTEP_DONE
        self.proc.result = sttr.success
        self.helper.save_proc(self.proc)

        self.proc.value += 5
        self.helper.save_proc(self.proc)

        self.log.info("SttKor::am_archive_result() - FINISH -")
        return True

    def lm_train(self):
        # step 1
        ret = self.lm_make_group()
        if not ret:
            self.log.critical("SttKor::lm_train() lm_make_group() fail !!!")
            return False

        # step 2
        ret = self.lm_make_list()
        if not ret:
            self.log.critical("SttKor::lm_train() lm_make_list() fail !!!")
            return False

        # step 3
        # LM 학습
        self.log.info("SttKor::lm_train() - STEP3 : LM Learning START -")
        self.proc.step = sttr.TSTEP_RUN_LM
        self.helper.save_proc(self.proc)

        ret = self.lm_do_make()
        if not ret:
            self.log.critical("SttKor::lm_train() lm_do_make() fail !!!")
            return False

        ret = self.lm_image_build()
        if not ret:
            self.log.critical("SttKor::lm_train() lm_image_build() fail !!!")
            return False

        ret = self.lm_copy_image()
        if not ret:
            self.log.critical("SttKor::lm_train() lm_copy_image() fail !!!")
            return False

        self.log.info("SttKor::lm_train() - STEP3 : LM Learning FINISH -")


        # step 4
        ret = self.lm_archive_result()
        if not ret:
            self.log.critical("SttKor::lm_train() lm_archive_result() fail !!!")
            return False

        return True

    # step 1
    # Make Lm Group
    def lm_make_group(self):
        self.log.info("SttKor::lm_make_group() - STEP1 : START -")

        self.proc.step = sttr.TSTEP_WAV_TO_PCM
        self.helper.save_proc(self.proc)

        # maum/workspace/stt-training-lm 밑에 작업Model 디렉토리 재구성
        path = os.path.join(self.stt_ws, self.stt_model.model)
        if not os.path.exists(path):
            os.makedirs(path)
        else:
            shutil.rmtree(path)
            os.makedirs(path)

        # list, all.list 파일 생성
        file_list = open(os.path.join(self.stt_ws, self.stt_model.model + '.list'), 'w')
        file_list_all_path = open(os.path.join(self.stt_ws, self.stt_model.model + '.all.list'), 'w')

        for file in self.stt_model.text_files:
            # LM의 경우 filename은 only file명만 있음
            file_hash = os.path.splitext(file.filename)[0]
            self.log.debug('SttKor::lm_make_group() file_hase : ' + file_hash)

            # create transcript file ( MUST EUC-KR )
            f = open(os.path.join(path, file_hash + '.txt'), 'wb')
            f.write(file.transcript.encode('euc-kr'))
            f.close()

            # enroll pcm file to file_list
            file_list.write(self.stt_model.model + '/' + file_hash + '.pcm' + '\n')
            file_list_all_path.write(os.path.join(path + '/' + file_hash + '.pcm' + '\n'))

        file_list.close()
        file_list_all_path.close()

        self.proc.value += 5
        self.helper.save_proc(self.proc)

        self.log.info("SttKor::lm_make_group() - STEP1 : FINISH -")
        return True

    # step 2
    # Make domain all list
    def lm_make_list(self):
        self.log.info("SttKor::lm_make_list() - STEP2 : START -")

        self.proc.step = sttr.TSTEP_MAKE_LIST
        self.helper.save_proc(self.proc)

        all_list = os.path.join(self.stt_ws, self.stt_model.model + '.all.list')

        self.log.debug('cmd> cp ' + all_list + ' ' + os.path.join(self.train_home, 'Release_LM_TrainEnv/input_data/train_set.txt'))
        os.system('cp ' + all_list + ' ' + os.path.join(self.train_home, 'Release_LM_TrainEnv/input_data/train_set.txt'))
        add_txt = os.path.join(self.stt_ws, self.stt_model.model + '.add.txt')

        # TODO  touch, cp 이 작업 필요한가 ? eng 는 없고 kor에서는 있지만 해당 파일을 참조하는 부분을 못 찾겠음
        '''
        os.system('touch ' + add_txt)
        self.log.debug('command> touch ' + add_txt)
        os.system('cp ' + add_txt + ' ' + os.path.join(self.train_home, 'Release_LM_TrainEnv/input_data/f_final.txt'))
        self.log.debug('command> cp ' + add_txt + ' ' + os.path.join(self.train_home, 'Release_LM_TrainEnv/input_data/f_final.txt'))
        '''

        self.proc.value += 5
        self.helper.save_proc(self.proc)
        self.log.info("SttKor::lm_make_list() - STEP2 : FINISH -")
        return True

    # step 4
    # LM 학습
    def lm_do_make(self):
        # do_make_lm_inc_test.sh
        os.chdir(os.path.join(self.train_home, 'Release_LM_TrainEnv/'))
        os.system(os.path.join(self.train_home, 'Release_LM_TrainEnv/do_make_lm_inc_test.sh') + ' ' + self.stt_model.model)
        self.proc.value += 2
        self.helper.save_proc(self.proc)
        os.system('cp ' + os.path.join(self.train_home, 'Release_LM_TrainEnv/lm_out_inc_test/stt_final.tri.lm') 
            + ' ' + os.path.join(self.train_home, 'Image_Build_Env/lm_out/stt_final.tri.lm'))
        os.system('cp ' + os.path.join(self.train_home, 'Release_LM_TrainEnv/wordlist_work/stt_final.voc') 
            + ' ' + os.path.join(self.train_home, 'Image_Build_Env/lm_out/stt_final.voc'))
        return True

    def lm_image_build(self):
        # do_image_build_dnn2.sh
        os.chdir(os.path.join(self.train_home, 'Image_Build_Env/'))
        os.system(os.path.join(self.train_home, 'Image_Build_Env/do_image_build_dnn2.sh') + ' ' + self.stt_model.model)
        return True

    def lm_copy_image(self):
        self.log.info("SttKor::lm_copy_image() - STEP3-3 : START -")
        train_workspace = os.path.join(self.stt_ws, self.helper.get_train_group_name())
        if not os.path.exists(train_workspace):
            os.makedirs(train_workspace)

        os.system('cp -r ' + os.path.join(self.train_home, 'dnn_image_out/stt_release.sfsm.bin') + ' ' 
            + os.path.join(train_workspace, 'sfsm.bin'))
        os.system('cp -r ' + os.path.join(self.train_home, 'dnn_image_out/stt_release.sym.bin') + ' ' 
            + os.path.join(train_workspace, 'sym.bin'))

        self.proc.value += 2
        self.helper.save_proc(self.proc)

        self.proc.value += 5
        self.helper.save_proc(self.proc)

        return True


    # result tar 압축
    def lm_archive_result(self):
        self.log.info("SttKor::archive_result() - START -")
        self.proc.step = sttr.TSTEP_ARCHIVE
        self.helper.save_proc(self.proc)

        os.chdir(self.stt_ws)
        os.system('tar -zcvf ' + self.helper.get_tar_filename() + ' ' + self.helper.get_train_group_name())

        train_data_dir = os.path.join(self.stt_train_data, self.helper.get_train_group_name())
        if not os.path.exists(train_data_dir):
            os.makedirs(train_data_dir)

        os.system('cp -r ' + self.helper.get_tar_work_dir() + '\/* ' + train_data_dir)

        self.proc.step = sttr.TSTEP_DONE
        self.proc.result = sttr.success
        self.helper.save_proc(self.proc)

        self.proc.value += 5
        self.helper.save_proc(self.proc)

        self.log.info("SttKor::archive_result() - FINISH -")
        return True
