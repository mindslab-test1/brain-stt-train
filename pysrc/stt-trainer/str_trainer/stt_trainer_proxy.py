# -*- coding:utf-8 -*-
import glob
import json
import os
import sys
import logger
import resource
import signal
import requests
from uuid import uuid4

from google.protobuf.timestamp_pb2 import Timestamp
from common.config import Config
from maum.common import lang_pb2
from maum.brain.stt.train import s3train_pb2 as sttr

from stt_trainer_helper import TrainerHelper
from str_trainer.eng.english_stt_trainer import EnglishSttTrainer
from str_trainer.kor.korean_stt_trainer import KoreanSttTrainer
from str_trainer.kor.korean_stt_trainer_16k import KoreanSttTrainer16k

class SttTrainerProxy:
    """
    공통 DNN Classifier Proxy
    """
    uuid = None
    pid = None
    helper = None
    callback_url = None
    fake_cnt = 0
    stt_model = sttr.SttModel()
    train_result = sttr.training
    started = Timestamp()
    gpu_idx = 0
    proc_name_ = os.path.basename(sys.argv[0]);
    log = logger.Create_Logger(os.path.splitext(proc_name_)[0], 'debug')

    def __init__(self, gpu_no):
        self.dic = {}
        self.uuid = uuid4()
        self.key = str(self.uuid)
        self.gpu_idx = gpu_no
        self.model = None
        self.proc = None
        self._TUTOR_API_KEY = '7c3a778e29ac522de62c4e22aa90c9'

    @staticmethod
    def _get_gpu_count():
        return len(os.listdir('/proc/driver/nvidia/gpus'))

    def init_proc(self):
        proc = sttr.TrainStatus()

        proc.result             = sttr.training
        proc.key                = str(self.uuid)
        proc.step               = sttr.TSTEP_START
        proc.model              = self.model.model
        proc.train_model_type   = self.model.train_model_type
        proc.deep_learning_type = self.model.deep_learning_type
        proc.lang               = self.model.lang
        proc.sample_rate        = self.model.sample_rate
        proc.maximum            = 35
        proc.value              = 0

        proc.started.GetCurrentTime()
        proc.elapsed.seconds = 0

        self.proc = proc
        self.helper.save_proc(self.proc)

    def run_trainer(self, stt_model):
        self.log = logger.Create_Logger(os.path.splitext(self.proc_name_)[0], 'debug')
        self.log.critical("run_trainer()")

        self.helper.save_model(stt_model)
        self.model = stt_model

        self.init_proc()

        trainer = None

        # 학습 모델 선택 (DNN/LSTM, KOR/ENG, 8000/16000, AM/LM)
        if stt_model.deep_learning_type == sttr.DEEP_LEARNING_DNN:
            if stt_model.lang == lang_pb2.kor:
                if stt_model.sample_rate == 8000:
                    self.log.info("run_trainer(DNN:KR) sample_rate is 8k")
                    trainer = KoreanSttTrainer(stt_model, self.helper, self.proc, self.log)
                elif stt_model.sample_rate == 16000:
                    self.log.info("run_trainer(DNN:KR) sample_rate is 16k")
                    trainer = KoreanSttTrainer16k(stt_model, self.helper, self.proc, self.log)
                else:
                    self.log.critical("run_trainer(DNN:KR) invalid rate : %d", stt_model.sample_rate)
                    return None
            elif stt_model.lang == lang_pb2.eng:
                if stt_model.sample_rate == 8000:
                    self.log.info("run_trainer(DNN:EN) sample_rate is 8k")
                    trainer = EnglishSttTrainer(stt_model, self.helper, self.proc, self.log)
                elif stt_model.sample_rate == 16000:
                    self.log.critical("run_trainer(DNN:EN) do not process DNN 16k Eng")
                    return None
                else:
                    self.log.critical("run_trainer(DNN:EN) invalid rate : %d", stt_model.sample_rate)
                    return None
            else:
                self.log.critical("run_trainer() invalid lang : %s", stt_model.lang)
                return None
        elif stt_model.deep_learning_type == sttr.DEEP_LEARNING_LSTM:
            self.log.critical("run_trainer() do not process LSTM")
            return None
        else:
            self.log.critical("run_trainer() invalid deep learning type : %d", stt_model.deep_learning_type)
            return None

        # 학습 시작
        if not trainer.do_training():
            self.proc.result = sttr.failed
            self.helper.save_proc(self.proc)
        else:
            self.train_result = sttr.success
            if self.key != None:
                self.finish_hook()
                self.key = None

    def open(self, stt_model):
        self.log.critical("STT_TRAINER::open()")

        self.callback_url = stt_model.callback_url
        """
        실지로 실행하는 학습 프로세스를 실행한다.
        학습 프로세스에서는 학습 실행 결과를 주기적으로 파일에 기록한다.
        빠를 실행 속도를 보장하기 위해서 위와 같이 처리한다.
        파일에 기록할 때 FILE LOCK을 실행한다.
        :param stt_model:
        :return:
        """
        self.helper = TrainerHelper(self.uuid, stt_model)
        newpid = os.fork()
        if newpid == 0:
            self.log.critical("STT_TRAINER::open() newpid %d", newpid)
            (cur, max) = resource.getrlimit(resource.RLIMIT_NOFILE)
            try:
                for fd in range(3, cur):
                    os.close(fd)
            except OSError, e:
                pass

            self.run_trainer(stt_model)
            return 0
            
        else:
            self.pid = newpid
            self.log.critical("STT_TRAINER::open() fork parent %d", newpid)
            return 1

    def finish_hook(self):
        self.log.critical("finish_hook()")
        if len(self.callback_url) == 0:
            self.log.critical("finish_hook() Invalid callback_url");
            return

        # 파일을 삭제한다.        self.helper.remove_proc()
        res = sttr.TrainResult.Name(self.train_result)
        #self.log.critical("finish_hook():: " + res + self.train_result)
        url = self.callback_url + '' + self._TUTOR_API_KEY
        self.log.critical("finish_hook():: call back url: " + url)
        cb_msg = {}
        cb_msg['message'] = 'Stt train job ' + self.key + ', result:' + res
        cb_msg['data'] = {
            'key': self.key,
            'name': self.model.model,
            'result': res,
            'binary': self.helper.get_tar_filename()
        }

        body = json.dumps(cb_msg)
        self.log.critical("finish_hook():: BODY : " + body)

        req = requests.post(url, data=body, headers={'Content-Type': 'application/json'})

    def get_key(self):
        return self.key

    def get_pid(self):
        return self.pid

    def get_progress(self):
        proc = self.helper.load_proc()
        try:
            self.train_result = proc.result
        except AttributeError, e:
            self.log.critical("get_progress():: msg => " + e.message)
        finally:
            return proc

    def get_binary(self):
        tar = self.helper.get_tar_file()
        if os.path.exists(tar):
            return self.bytes_from_file(tar)
        else:
            return None

    @staticmethod
    def bytes_from_file(filename, chunksize=1024 * 1024):
        with open(filename, "rb") as f:
            while True:
                chunk = f.read(chunksize)
                if chunk:
                    tar = sttr.TrainBinary()
                    tar.tar = chunk
                    yield tar
                else:
                    break

    @staticmethod
    def get_binary_by_key(uuid, context, grpc):
        conf = Config()
        ws = conf.get('brain-stt.trained.root')
        find = ws + '/*' + uuid + '.sttimage.tar.gz'
        print "get_binary_by_key():: find => %s" %find
        files = glob.glob(find)
        if len(files) > 0:
            return SttTrainerProxy.bytes_from_file(filename=files[0])
        else:
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details('train id not found')
            raise grpc.RpcError("not found")

    @staticmethod
    def remove_binary_by_key(uuid):
        conf = Config()
        ws = conf.get('brain-stt.trained.root')
        find = ws + '/*' + uuid + '.sttimage.tar.gz'
        files = glob.glob(find)
        for f in files:
            self.log.critical("remove_binary_by_key:: " + uuid + f)
            os.remove(f)

    def cancel(self):
        proc = self.helper.load_proc()
        if proc.result == sttr.training:
            os.kill(self.pid, signal.SIGTERM)
            self.train_result = sttr.cancelled
            proc.result = sttr.cancelled
            self.helper.save_proc(proc)

    def set_result(self, res):
        self.log.critical("set_result:: " + sttr.TrainResult.Name(res))
        self.train_result = res
