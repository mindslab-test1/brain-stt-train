#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os, sys, logger, uuid
import signal
import grpc

from google.protobuf.empty_pb2 import Empty
from common.config import Config
from maum.common import lang_pb2
from maum.brain.stt.train import s3train_pb2 as sttr
from maum.brain.stt.train import s3train_pb2_grpc as sttrg

from stt_trainer_proxy import SttTrainerProxy

exe_path = os.path.realpath(sys.argv[0])
bin_path = os.path.dirname(exe_path)
lib_path = os.path.realpath(bin_path + '/../lib/python')
sys.path.append(lib_path)

class SttTrainer(sttrg.SttTrainerServicer):
    conf = Config()
    trainers = {}
    childs = {}
    # gpu round robin
    gpu_count = 0
    next_gpu = 0
    proc_name_ = os.path.basename(sys.argv[0]);
    log = logger.Create_Logger(os.path.splitext(proc_name_)[0], 'debug')

    def _get_gpu_count(self):
        try:
            self.gpu_count = len(os.listdir('/proc/driver/nvidia/gpus'))
        except:
            self.gpu_count = 0
            self.log.critical('%s', 'No gpu exists')
            sys.exit(2)

    def _next_gpu_index(self):
        ret = self.next_gpu
        self.next_gpu += 1
        if self.next_gpu >= self.gpu_count:
            self.next_gpu = 0

        self.log.info("Train_Server::NEXT GPU() ret=%d, next_gpu=%d, count=%d", ret, self.next_gpu, self.gpu_count)
        return ret

    def _handle_signal(self, signal, frame):
        self.log.critical("*" * 80);
        self.log.critical('*%30s : %s', "STOP", self.proc_name_);
        self.log.critical("*" * 80);
        self.log.critical('');
        sys.exit(0)


    def __init__(self):
        signal.signal(signal.SIGTERM, self._handle_signal)
        signal.signal(signal.SIGSEGV, self._handle_signal)
        signal.signal(signal.SIGINT, self._handle_signal)
        signal.signal(signal.SIGBUS, self._handle_signal)

        self.log.critical('Train_Server::initializing...')
        self._get_gpu_count()
        self.log.critical("gpu count : %d", self.gpu_count)
        pass

    def Open(self, stt_model, context):
        self.log.critical("Train_Server::Open()")
        trainer = SttTrainerProxy(self._next_gpu_index())
        ret = trainer.open(stt_model)
        if ret == 1:
            open_res = sttr.TrainKey()
            open_res.train_id = trainer.get_key()
            self.log.critical("Train_Server::Open() TRAIN_KEY : %s", trainer.get_key())
            self.log.critical("Train_Server::Open() TRAIN_PID : %s", trainer.get_pid())
            self.trainers[trainer.get_key()] = trainer
            self.childs[trainer.get_pid()] = trainer
            return open_res
        else:
            pass

    def OpenStream(self, stt_stream_model_iter, context):
        stream_count = 0

        self.log.critical("OpenStream()")

        # STT_MODEL Data Struct 재구성
        stt_model = sttr.SttModel()
        for m in stt_stream_model_iter:
            if stream_count == 0:
                self.log.critical("OpenStream() init stt_model")
                stt_model.model              = m.param.model
                stt_model.train_model_type   = m.param.train_model_type
                stt_model.deep_learning_type = m.param.deep_learning_type
                stt_model.lang               = m.param.lang
                stt_model.sample_rate        = m.param.sample_rate
                stt_model.callback_url       = m.param.callback_url
                stream_count += 1

            if len(m.audio_files.filename) > 0:
                self.log.debug("OpenStream() insert audio_file[%s]", m.audio_files.filename)
                stt_model.audio_files.extend([m.audio_files])
                stream_count += 1

            if len(m.text_files.filename) > 0:
                self.log.debug("OpenStream() insert text_file [%s]", m.text_files.filename)
                stt_model.text_files.extend([m.text_files])
                stream_count += 1
        
        # INVALID CHECK
        if len(stt_model.model) == 0:
            self.log.critical("OpenStream() Invalid Model Name");
            
        if ((stt_model.train_model_type != sttr.TRAIN_MODEL_ALL) and
            (stt_model.train_model_type != sttr.TRAIN_MODEL_AM) and
            (stt_model.train_model_type != sttr.TRAIN_MODEL_LM)):
            self.log.critical("OpenStream() Invalid train_model_type(%d)", stt_model.train_model_type);

        if ((stt_model.deep_learning_type != sttr.DEEP_LEARNING_DNN) and
            (stt_model.deep_learning_type != sttr.DEEP_LEARNING_LSTM)):
            self.log.critical("OpenStream() Invalid deep_learning_type(%d)", stt_model.deep_learning_type);

        if ((stt_model.lang != lang_pb2.kor) and
            (stt_model.lang != lang_pb2.eng)):
            self.log.critical("OpenStream() Invalid lang(%d)", stt_model.lang);

        if ((stt_model.sample_rate != 8000) and
            (stt_model.sample_rate != 16000)):
            self.log.critical("OpenStream() Invalid sample_rate(%d)", stt_model.sample_rate);

        if len(stt_model.callback_url) == 0:
            self.log.critical("OpenStream() callback_url is NULL");
            
        if stream_count <= 1:
            self.log.critical("OpenStream() Invalid files")
            context.set_code(grpc.StatusCode.FILE_NOT_FOUND)
            context.set_details('FILE NOT FOUND')
            raise grpc.RpcError("FILE NOT FOUND")
        
        # PRINT RECV DATA
        self.log.critical("* RECV *" + "*" * 70)
        self.log.critical("stt_model.model              : %s", stt_model.model)
        self.log.critical("stt_model.train_model_type   : %d (0:ALL, 1:AM, 2:LM)", stt_model.train_model_type)
        self.log.critical("stt_model.deep_learning_type : %d (0:DNN, 1:LSTM)", stt_model.deep_learning_type)
        self.log.critical("stt_model.lang               : %s", lang_pb2.LangCode.Name(stt_model.lang))
        self.log.critical("stt_model.sample_rate        : %d", stt_model.sample_rate)
        self.log.critical("stt_model.stream count       : %d", stream_count)
        self.log.critical("stt_model.callback_url       : %s", stt_model.callback_url)
        self.log.critical("*" * 80)

        trainer = SttTrainerProxy(self._next_gpu_index())
        ret = trainer.open(stt_model)
        if ret == 1:
            self.log.critical("OpenStream() Parent")
            open_stream_res = sttr.TrainKey()
            open_stream_res.train_id = trainer.get_key()
            self.log.critical("OpenStream() TRAIN_KEY : %s", trainer.get_key())
            self.log.critical("OpenStream() TRAIN_PID : %s", trainer.get_pid())
            self.trainers[trainer.get_key()] = trainer
            self.childs[trainer.get_pid()] = trainer
            return open_stream_res
        else:
            pass

    def GetProgress(self, key, context):
        self.log.info("Train_Server::GetProgress()")
        if key.train_id in self.trainers:
            self.log.critical("Train_Server::GetProgress() TRAIN_KEY : %s", key.train_id)
            trainer = self.trainers[key.train_id]
            return trainer.get_progress()
        else:
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details('train id not found')
            raise grpc.RpcError("not found")

    def GetAllProgress(self, empty, context):
        self.log.debug("Train_Server::GetAllProgress()")
        ret = sttr.TrainStatusList()
        res = []
        for tr in self.trainers.itervalues():
            res.append(tr.get_progress())
        ret.trains.extend(res)
        return ret

    def GetBinary(self, key, context):
        self.log.debug("Train_Server::GetBinary() key = %s", str(key.train_id))
        if key.train_id in self.trainers:
            trainer = self.trainers[key.train_id]
            return trainer.get_binary()
        else:
            return SttTrainerProxy.get_binary_by_key(str(key.train_id), context, grpc)

    def RemoveBinary(self, key, context):
        self.log.debug("Train_Server::RemoveBinary()")
        SttTrainerProxy.remove_binary_by_key(str(key.train_id))
        return Empty()

    def Close(self, key, context):
        self.log.debug("Train_Server::Close() key = %s", str(key.train_id))
        if key.train_id in self.trainers:
            trainer = self.trainers[key.train_id]
            ret = trainer.get_progress()
            del self.trainers[key.train_id]
            pid = self.childs.keys()[self.childs.values().index(trainer)]
            del self.childs[pid]
            self.log.critical( 'NOW delete proxy trainer pid:%s', trainer.get_pid())
            del trainer
            return ret
        else:
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details('train id not found')
            raise grpc.RpcError("not found")

    def Stop(self, key, context):
        self.log.debug("Train_Server::Stop()")
        id = str(key.train_id)
        trainer = self.trainers[id]
        if trainer != None:
            ret = trainer.get_progress()
            trainer.cancel()
            return ret
        else:
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details('train id not found')
            raise grpc.RpcError("not found")

    def update_proc(self, proc_stat):
        self.log.debug("Train_Server::update_proc() proc_stat.key = %s", str(proc_stat.key))
        """
        child에서 종료할 때, 해당 상태를 정리한다.

        :param proc_stat: proc의 상태 값을 전달한다.
        """
        key = str(proc_stat.key)
        if key in self.trainers:
            trainer = self.trainers[key]
            pid = self.childs.keys()[self.childs.values().index(trainer)]
            assert pid == proc_stat.pid
            del self.trainers[key]
            del self.childs[pid]
            # 상태를 기록해준다.
            trainer.set_result(proc_stat.result)
            self.log.critical( 'NOW delete proxy trainer pid:%s', trainer.get_pid())
            del trainer
