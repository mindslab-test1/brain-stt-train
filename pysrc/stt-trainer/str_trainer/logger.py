#!/usr/bin/python
# -*- coding: utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 2018-05-07, modification: 0000-00-00"

###########
# imports #
###########
import os
import sys
import logging
import logging.handlers

#######
# def #
#######
def Create_Logger(proc_name, text_log_level):
    # create logger
    if not os.path.exists(os.getenv('MAUM_ROOT') + '/logs'):
        os.makedirs(os.getenv('MAUM_ROOT') + '/logs')

    logger = logging.getLogger(proc_name+str(os.getpid()));

    # Check handler exists
    if len(logger.handlers) > 0 :
        return logger # Logger already exists

    # Set log level
    if text_log_level.lower() == 'info':
        log_level = 20
    elif text_log_level.lower() == 'warning':
        log_level = 30
    elif text_log_level.lower() == 'error':
        log_level = 40
    elif text_log_level.lower() == 'critical':
        log_level = 50
    else:
        log_level = 10

    logger.setLevel(log_level)

    # create formatter
    formatter = logging.Formatter(
        fmt='[%(asctime)s.%(msecs)03d][%(levelname).3s] %(message)s', datefmt='%H:%M:%S'
    )

    # Add ch handler
    ch = logging.handlers.TimedRotatingFileHandler(
        os.path.join(os.getenv('MAUM_ROOT') + '/logs', proc_name),
        when='midnight',
        interval=1,
        backupCount=7,
        encoding=None,
        delay=False,
        utc=False
    )
    ch.setLevel(log_level)
    ch.setFormatter(formatter) # Add formatter to ch
    ch.suffix = "%Y%m%d"
    logger.addHandler(ch)

    # Add StreamHandler to logger
    st = logging.StreamHandler(sys.stdout)
    st.setFormatter(formatter)
    st.suffix = "%Y%m%d"
    st.setLevel(logging.DEBUG)
    logger.addHandler(st)
    return logger
